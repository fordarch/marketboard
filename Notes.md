============================================================================
BACKEND
============================================================================
CONVENTION: All entities (database, tables, columns, etc) are named singular by default; however, plural table names suggest a one-to-many relationship.
    e.g.
        item contains a fk in item property
        o item(sword) - item_property(weight)
        
        item may contain many attributes
        o item(sword) -= weapon_attributes(light, martial)

CONVENTION: lower-case casing with snake-casing, when appropriate, everywhere.



ITEM
--------------------------------------------
ID         | NAME   | WEIGHT | DESCRIPTION
0000000000 | Sword  | 10     | Sweet Sword
0000000001 | Basket | 1      | Nice Basket  
0000000002 | Shield | 15     | Sturdy Sheild  
--------------------------------------------


ITEM ATTRIBUTES
-------------------------------------------------------------------------------
ID          | ITEM ID    | ATTRIBUTE    | DESCRIPTION
0000000002  | 0000000001 | Capacity    | Can carry up to 15kg of fitting items.
-------------------------------------------------------------------------------


ARMOR 
---------------------------------------------------------------
ID          | ITEM ID    | SIZE_CATEGORY  | MATERIAL | AC_BONUS  
0000000000  | 0000000002 | Medium         | Steel    | 2        
---------------------------------------------------------------

ARMOR ATTRIBUTES
----------------------------------------------------------------------------------------------------
ID          | ARMOR ID    | ATTRIBUTE | DESCRIPTION
0000000000  | 0000000000 | Light    | A light weapon is small and easy to handle.
0000000001  | 0000000000 | Martial  | Martial Weapons require training to use effectively in combat.    
----------------------------------------------------------------------------------------------------



WEAPON 
---------------------------------------------------------------------------
ID          | ITEM ID    | SIZE_CATEGORY  | MATERIAL | DAMAGE | DAMAGE_TYPE 
0000000000  | 0000000000 | Medium         | Steel    | D6     | S
---------------------------------------------------------------------------

WEAPON ATTRIBUTES
----------------------------------------------------------------------------------------------------
ID          | WEAPON ID    | ATTRIBUTE | DESCRIPTION
0000000000  | 0000000000 | Light    | A light weapon is small and easy to handle.
0000000001  | 0000000000 | Martial  | Martial Weapons require training to use effectively in combat.    
----------------------------------------------------------------------------------------------------





POSTINGS (DETAIL)
-----------------------------------------------------------------------
LISTING ID  | ITEM ID     | POSTED QUANTITY | UNIT COST | SELLER ID  
0000000000  | 0000000000  | 10              | 10        | 0000000000 
0000000001  | 0000000000  | 20              | 10        | 0000000001 
0000000002  | 0000000000  | 30              | 20        | 0000000001 
0000000003  | 0000000000  | 40              | 25        | 0000000000 
0000000004  | 0000000001  | 30              | 20        | 0000000001 
0000000005  | 0000000002  | 40              | 25        | 0000000000 
-----------------------------------------------------------------------


POSTINGS (SUMAMRY DATA) (MATERIALIZED VIEW)
-----------------------------------------------------------------------
ITEM ID     | ITEM NAME  | TOTAL POSTED QUANTITY   
0000000000  | Sword      | 100                      
0000000001  | Basket     | 30                        
0000000002  | Shield     | 40                            
-----------------------------------------------------------------------


USERS
-----------------------------------------------------------------------
USER ID   | USER NAME   
000000000 | Alice      
-----------------------------------------------------------------------


============================================================================
FRONTEND 
============================================================================

THE USER WILL VIEW THE POSTINGS THIS WAY
-------------------------------------------------------------------------------
ITEM  | ITEM DESCRIPTION                | TOTAL AVAILABLE QUANTITY  
Sword | Size: Medium, Material: Steel   | 100                 
-------------------------------------------------------------------------------

    (FOR SELECTED ITEM)
    -------------------------------
    UNIT COST | AVAILABLE QUANTITY 
    10        | 10                  
    15        | 20                  
    20        | 30                  
    30        | 40                  
    -------------------------------


============================================================================
SCIENCE!
============================================================================

ITEMS
-------------------
ID         | NAME  
0000000000 | Sword 
0000000001 | Basket      
0000000002 | Shield      
-------------------


ITEM ATTRIBUTES
-------------------------------------------------
ID          | ITEM ID    | WEIGHT | DESCRIPTION 
0000000000  | 0000000000 | 10     | Sweet Sword
0000000001  | 0000000001 | 1      | Nice Basket
0000000002  | 0000000002 | 15     | Sturdy Sheild
-------------------------------------------------

ITEM PROPERTIES
-------------------------------------------------------------------------------
ID          | ITEM ID    | PROPERTY    | DESCRIPTION
0000000002  | 0000000001 | Capacity    | Can carry up to 15kg of fitting items.
-------------------------------------------------------------------------------


ARMOR ATTRIBUTES
---------------------------------------------------------------
ID          | ITEM ID    | SIZE_CATEGORY  | MATERIAL | AC_BONUS  
0000000000  | 0000000002 | Medium         | Steel    | 2        
---------------------------------------------------------------

ARMOR PROPERTIES
----------------------------------------------------------------------------------------------------
ID          | ITEM ID    | PROPERTY | DESCRIPTION
0000000000  | 0000000002 | Light    | A light weapon is small and easy to handle.
0000000001  | 0000000002 | Martial  | Martial Weapons require training to use effectively in combat.    
----------------------------------------------------------------------------------------------------



WEAPON ATTRIBUTES
---------------------------------------------------------------------------
ID          | ITEM ID    | SIZE_CATEGORY  | MATERIAL | DAMAGE | DAMAGE_TYPE 
0000000000  | 0000000000 | Medium         | Steel    | D6     | S
---------------------------------------------------------------------------

WEAPON PROPERTIES
----------------------------------------------------------------------------------------------------
ID          | ITEM ID    | PROPERTY | DESCRIPTION
0000000000  | 0000000000 | Light    | A light weapon is small and easy to handle.
0000000001  | 0000000000 | Martial  | Martial Weapons require training to use effectively in combat.    
----------------------------------------------------------------------------------------------------




============================================================================
THOUGHTS
============================================================================
---------------------------------------------------------
One database instance assumes a single campaign setting.
---------------------------------------------------------

---------------------------------------------------------
What do the views look like to the user when --
- buying
    o buyer can browse the sumary data list of items and their associated available quantity
    o the summary list provides each item's id
    o using the item id a buyer can view the detail postings grouped by the target item 
    o each posting will need an id the buyer will use when making a purchase
    o listing id will be used by the buying to identify the item to bid on (id := item name, seller, posted price)
- selling
    o seller will browse for items retrieving the item id for which the seller wishes to post
    o user submits the item id, the quantity of item to sell, the price per unit they wish to sell the item, their seller's id (user id)
---------------------------------------------------------


---------------------------------------------------------
messy, tricky, unorthodox
addresses complexity of high-order combinitorials of item properties (weapons, gear, armor)

Use a numerical/binary reference for properties
1 = 0000000001 := 
[
    {reach-weapon: 0},
    {double-weapon: 0},
    {thrown-weapon: 0},
    ...
    {one-handed-weapon: 1}
]

All items in item table created with single binary/int column representing properties.
Properties lookup table for only the flat, non-combination, values; 
this means, for 10 bits/decimal places, there are 10 rows and only ever 2 columns.

Expected use is to to cross-reference items with any number of arbitrary properties;
used by the application to extrapolate all properties

---------------------------------------------------------
