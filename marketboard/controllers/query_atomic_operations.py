""" Operations leveraging the query elements to statify requests from query operations.
    Operations here are distinctly atomic operations for query logic.
"""
