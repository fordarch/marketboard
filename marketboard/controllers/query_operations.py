""" Operations leveraging the query elements to statify requests from query operations.
"""

""" In this module is the application that contain the processing and coupling for the database.
    The API requests go to this application.
    This application couples with the database and the two must be managed to stay syncronized. 
    This is where common API requests are cached.
    This is where the 'marketboard' navtree lives.
    Changes to the database demand review of the application.
    Changes to the application demand review of the database.
"""


def select_item_postings(item_id):
    """ Return all postings/records belonging to a specific item id.
        database:   marketboard
        tables:     [ITEM]
        columns:    [id, name, weight, description]
        operation:  SELECT
    """
    # TODO: here would be an OK place to validate and santize user-passed paramters.
    # item_id of type int (we format to str when passing psycopg), int val >= 0 and <= n.
    query = """
            SELECT id, name, weight, description
            FROM item
            WHERE item.id == %s
            """
    params = (item_id,)

    execute_simple_sql(query, params)


def insert_item_posting(name, weight, description):
    return


def placeNewOffer(data):
    for d in data:
        print(d)

def placeNewBid(data):
    raise NotImplementedError("Under Construction")


def main():
    """ A main method while until we have tests
    """
    select_item_postings(0)


if __name__ == '__main__':
    main()
