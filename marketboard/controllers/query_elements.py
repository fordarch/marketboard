""" Single, simple, queries from which operations that require multiple queries can be built.
"""
# TODO: Consider using 'row factories' as specified/suggested by psycopg documentation.

import psycopg


def select_item_postings(item_id):
    """ Return all postings/records belonging to a specific item id.
        database:   marketboard
        tables:     [ITEM]
        columns:    [id, name, weight, description]
        operation:  SELECT
    """
    # TODO: here would be an OK place to validate and santize user-passed paramters.
    # item_id of type int (we format to str when passing psycopg), int val >= 0 and <= n.
    query = """
            SELECT id, name, weight, description
            FROM item
            WHERE item.id == %s
            """
    params = (item_id,)

    execute_simple_sql(query, params)


def insert_item_posting(name, weight, description):
    """ Return all postings/records belonging to a specific item id.
        database:   marketboard
        tables:     [ITEM]
        columns:    [id, name, weight, description]
        operation:  INSERT
    """
    # TODO: here would be a great place to validate and santize user-passed paramters.
    query = """
            INSERT INTO test 
            (num, data) 
            VALUES 
            (%s, %s, %s)
            """
    params = (name, weight, description)

    execute_simple_sql(query, params)


def execute_simple_sql(query):
    db_name = 'marketboard'
    db_user = 'zeke'
    db_pass = 'a great password'
    db_addr = 'resolvable url addr'

    with psycopg.connect(f'dbname={db_name} user={db_user}') as conn:
        with conn.cursor() as cur:
            cur.execute(query)

            # Note: 
            # fetchall returns a list of tuples
            # e.g.
            # (index, first_column_value, second_column_value)
            # (1, 100, "abc'def")

            cur.fetchall()

            for record in cur:
                print(record)


def main():
    """ A main method while until we have tests
    """
    select_item_postings(0)


if __name__ == '__main__':
    main()
