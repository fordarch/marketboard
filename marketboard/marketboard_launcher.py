"""
"""

import marketboard.routes.marketboard_api as api



def open_api():
    api.main()


def main():
    # TODO: Setup/init checks here.
    open_api()


if __name__ == '__main__':
    main()
