import http
import json
from flask import Flask, request, jsonify
from waitress import serve
from  marketboard.controllers import offer, bid, postings


app = Flask(__name__)
_version = (json.dumps({'version': '0.1'}),http.HTTPStatus.OK) 


@app.route("/offer/new", methods=['POST'])
def post_offer_new():
    """ Post a new item for sale.
    """
    offer.post_new_offer(request.json)
    return ('',http.HTTPStatus.NO_CONTENT)


@app.route("/bid/new", methods=['POST'])
def post_bid_new():
    """ Place a bid on an existing item.
    """
    bid.post_new_bid(request.json)
    return ('',http.HTTPStatus.NO_CONTENT)


@app.route("/postings/navtree", methods=['GET'])
def get_postings_navtree():
    """ Provide the navigation tree for items.
        Navigation tree provides heirarchy of items by catagory.  
    """
    postings.get_postings_navtree(request.json)
    return _version


@app.route("/postings/all", methods=['GET'])
def get_marketboard_postings():
    """ Provide the current postings for an item by ID. 
    """
    requested_item_id = request.args.get('id')
    postings.get_postings_all(request.json)
    return _version


@app.route("/marketboard/version", methods=['GET'])
def get_marketboard_version():
    """ Return this version of the marketboard API.
    """
    return _version


def main():
    serve(app, host='0.0.0.0', port=5000, threads=4)


if __name__ == '__main__':
    # Running this Flask application will automatically launch Waitress (WSGI) which serves the Flask app.
    # Alternative, command line, waitress hosting: $waitress-serve.exe --port=5000 streamProcessing:app
    # For Flask testing/debugging, bypass waitress: app.run(host='127.0.0.1', port=5000, threaded=True)

    #import logging
    #logger = logging.getLogger('waitress')
    #logger.setLevel(logging.DEBUG)
    main()
