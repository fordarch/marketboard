----------------------------------------------------------------------------------
-- In this script, we populate an empty database.
--   Script was created to populate the database upon first creation.
-- This script matches the DDL of the database matching the timestamp 2022-12-25.
----------------------------------------------------------------------------------

-- The ITEM table must first be populated 
--   An item is identified when it is joined with its properties contained within the correct table.
--   For example, a sword is just an item until it is joined with its properties contained within the weapons table.

INSERT INTO backend.item
(id, "name", weight, description)
VALUES(uuid_generate_v4(), 'sword', 10, 'a sword');


-- Populate the weapons table and match the ID values of each associated item.

insert 
into marketboard.backend.weapon 
(id, item_id, size_category, material, damage, damage_type)
values 
(
	uuid_generate_v4(), 
	(select item.id from backend.item where item."name" = 'sword'),
	'medium', 'steel', 'd6', 'slashing');


-- Post the offer of a new sword to sell

INSERT INTO backend.postings
(id, item_id, quantity, unit_cost, seller_id)
VALUES
(
	uuid_generate_v4(), 
	(select item.id from backend.item where item."name" = 'sword'), 
	0, 
	0, 
	uuid_generate_v4()
);