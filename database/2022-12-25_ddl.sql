-- DROP SCHEMA backend;

CREATE SCHEMA backend AUTHORIZATION postgres;
-- backend.item definition

-- Drop table

-- DROP TABLE backend.item;

CREATE TABLE backend.item (
	id uuid NOT NULL,
	"name" varchar NOT NULL,
	weight int4 NOT NULL,
	description varchar NULL,
	CONSTRAINT item_pk PRIMARY KEY (id),
	CONSTRAINT item_un UNIQUE (name)
);


-- backend.armor definition

-- Drop table

-- DROP TABLE backend.armor;

CREATE TABLE backend.armor (
	id uuid NOT NULL,
	item_id uuid NOT NULL,
	size_category varchar NOT NULL,
	material varchar NOT NULL,
	ac_bonus varchar NOT NULL,
	CONSTRAINT armor_property_un UNIQUE (item_id),
	CONSTRAINT armor_property_pk PRIMARY KEY (id),
	CONSTRAINT armor_fk FOREIGN KEY (item_id) REFERENCES backend.item(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX armor_property_item_id_idx ON backend.armor USING btree (item_id);


-- backend.armor_attributes definition

-- Drop table

-- DROP TABLE backend.armor_attributes;

CREATE TABLE backend.armor_attributes (
	id uuid NOT NULL,
	armor_id uuid NOT NULL,
	"attribute" varchar NOT NULL,
	description varchar NULL,
	CONSTRAINT armor_attributes_pk PRIMARY KEY (id),
	CONSTRAINT armor_attributes_fk FOREIGN KEY (armor_id) REFERENCES backend.armor(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX armor_attributes_armor_id_idx ON backend.armor_attributes USING btree (armor_id);


-- backend.weapon definition

-- Drop table

-- DROP TABLE backend.weapon;

CREATE TABLE backend.weapon (
	id uuid NOT NULL,
	item_id uuid NOT NULL,
	size_category varchar NOT NULL,
	material varchar NOT NULL,
	damage varchar NOT NULL,
	damage_type varchar NOT NULL,
	CONSTRAINT weapon_pk PRIMARY KEY (id),
	CONSTRAINT weapon_un UNIQUE (item_id),
	CONSTRAINT weapon_fk FOREIGN KEY (item_id) REFERENCES backend.item(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX weapon_item_id_idx ON backend.weapon USING btree (item_id);


-- backend.weapon_attributes definition

-- Drop table

-- DROP TABLE backend.weapon_attributes;

CREATE TABLE backend.weapon_attributes (
	id uuid NOT NULL,
	weapon_id uuid NOT NULL,
	"attribute" varchar NOT NULL,
	description varchar NULL,
	CONSTRAINT weapon_attributes_pk PRIMARY KEY (id),
	CONSTRAINT weapon_attributes_fk FOREIGN KEY (weapon_id) REFERENCES backend.weapon(id)
);
CREATE INDEX weapon_attributes_item_id_idx ON backend.weapon_attributes USING btree (weapon_id);


-- backend.item_attributes definition

-- Drop table

-- DROP TABLE backend.item_attributes;

CREATE TABLE backend.item_attributes (
	id uuid NOT NULL,
	item_id uuid NOT NULL,
	"attribute" varchar NOT NULL,
	description varchar NULL,
	CONSTRAINT item_attributes_pk PRIMARY KEY (id),
	CONSTRAINT item_attributes_fk FOREIGN KEY (item_id) REFERENCES backend.item(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX item_attributes_item_id_idx ON backend.item_attributes USING btree (item_id);

-- DROP SCHEMA frontend;

CREATE SCHEMA frontend AUTHORIZATION postgres;


-- Add extension for UUID generation

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";