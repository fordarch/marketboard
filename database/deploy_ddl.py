""" In this module are tools for deploying the DDL to the database.
    This module works by using a provided DDL SQL script. 
"""

import psycopg


# TODO: Break this work up. Using main() for initial design and prototyping.
def main():
    db_name = 'marketboard'
    db_user = 'zeke'
    db_pass = 'a great password'
    db_addr = 'resolvable url addr'
    sql_ddl = './2022-12-25_ddl.sql'
    sql_data = './2022-12-25_populate.sql'

    sql_ddl_text = ''
    with open(sql_ddl) as sql_ddl_file:
        sql_ddl_text = sql_ddl_file.readlines()

    sql_data_text = ''
    with open(sql_data) as sql_data_file:
        sql_data_text = sql_data_file.readlines()
    

    with psycopg.connect(f'dbname={db_name} user={db_user}') as conn:
        with conn.cursor() as cur:
            cur.execute(sql_ddl_text)

            cur.execute(sql_data_text)

            # How do you suppose we can do QA here?
            # query the database for the expected result that we expect to result from our database deployment?
            cur.execute("SELECT * FROM item")
            
            cur.fetchone()
            
            for record in cur:
                print(record)

            conn.commit()


if __name__ == '__main__':
    main()